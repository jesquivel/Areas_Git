import math

def area_esfera(Radio):
    """############################################################################
Jarod De la O Segura
Jonathan Esquivel Sánchez
Version:1.0.0
############################################################################"""
    
    if Radio > 0:
        P=4*math.pi*Radio**2
        return P
    else:
        return "El radio tiene que ser mayor que 0"


def area_piramide(LadoBase,Altura):
    """############################################################################
Jarod De la O Segura
Jonathan Esquivel Sánchez
Version:1.0.0
############################################################################"""

    if LadoBase>0 and Altura > 0:
        AP=(4*LadoBase)*math.sqrt((LadoBase/2)**2+altura**2)
        return AP
    else:
        return "El lado y la altura tienen que ser mayores que 0"

def vol_esf(r):
    """############################################################################
Jarod De la O Segura
Jonathan Esquivel Sánchez
Version:1.0.0
############################################################################"""
    import math
    import pi
    if r>0:
	volumen=(4/3)*pi*r**3
	return volumen
    else:
        return "El valor del radio debe ser mayor que cero"

def vol_piramide(ladobase,ancho,altura):
      """############################################################################
Jarod De la O Segura
Jonathan Esquivel Sánchez
Version:1.0.0
############################################################################"""

    volumen=(ladobase*ancho*altura)/3
    if ladobase >0 and ancho>0 and altura>0:
        return volumen
    else:
        return "El valor del radio debe ser mayor que cero"    
